#import "@preview/polylux:0.3.1": *
#import "@preview/pinit:0.1.4": *
#import "@preview/showybox:2.0.1": showybox
#import calc

#let defaultbg = rgb("#eceff4")
#let defaultfg = rgb("#2e3440")

#let nordred    = rgb("#bf616a")
#let nordorange = rgb("#d08770")
#let nordyellow = rgb("#ebcb8b")
#let nordgreen  = rgb("#a3be8c")
#let nordpurple = rgb("#b48ead")

#let nordsnow0  = rgb("#eceff4")
#let nordsnow1  = rgb("#e5e9f0")
#let nordsnow2  = rgb("#d8dee9")

#let highlightcolor = rgb("#5e81ac")
#let bordercolor = highlightcolor
#let wellcolor   = rgb("#d8dee9")
#let wellheader  = highlightcolor
#let wellheaderfg  = defaultbg
#let footercolor = rgb("#8fbcbb")


#let bodyfont = "Heliotrope OT 3"
#let headerfont = "Heliotrope OT 4"
#let wellfont = "Heliotrope OT 6"
#let footerfont = "Heliotrope OT 4"
#let ttfont = "MonoLisa"

#let yes   = image("icon-yes.svg", height: 1em)
#let no    = ""
#let buggy = image("icon-buggy.svg", height: 1em)
#let other = image("icon-other.svg", height: 1em)
#let orig  = image("icon-original.svg", height: 1em)

#set page(paper: "presentation-16-9", fill: defaultbg,
  footer: align(end)[#text( fill: footercolor, font: footerfont )[#logic.logical-slide.display() / #utils.last-slide-number]])
#set text(size: 18pt, font: bodyfont, weight: "medium", fill: defaultfg, features: ("onum",))
#set par(leading: 1.4em)

#let logflux = text(font: headerfont)[#smallcaps[LogFlux]]

#let well(title,body) = showybox(
    frame: (
        border-color: bordercolor,
        title-color:  wellheader,
        body-color:   wellcolor,
        inset: 10pt,
    ),
    title-style: (
        boxed-style: (radius: 5pt),
        color: wellheaderfg,
    ),
    title: text(font: wellfont, weight: "regular")[#smallcaps(title)],
    body
  )

#let hirect(w,h) = rect(width: w, height: h, stroke: 3pt + nordred, radius: 10pt)
#let lorect(w,h) = rect(width: w, height: h, stroke: 3pt + nordgreen, radius: 10pt)

#let bigtext(body) = text(
  fill: highlightcolor,
  size: 96pt,
  font: headerfont,
  [
    #v(1fr)
    #h(1fr) #body #h(1fr)
    #v(1fr)
  ]
)

#show link: underline
#set list(marker: ([#text(fill:highlightcolor, weight: 700)[•]],[#text(fill: highlightcolor, weight: 700)[-]]))

#show heading: it => [
  #set text(font: headerfont, fill: highlightcolor)
  #smallcaps(it)
  #v(1em)
]

#show raw: it => {
  set text(font: ttfont, size: 12pt)
  show regex("pin\d\d"): it => pin(eval(it.text.slice(3)))
  it
}

#let labelthing(img, label, width: 3cm) = {
  align(center)[#image(img,width: width)
  #v(-0.7cm)
  === #label]
}

#let icon(img,body) = [
  #box(move(image(img, height: 1.0em),dy:0.15em)) #h(0.2em) #body
]

#polylux-slide[
  #set align(horizon + center)

  #text(fill: highlightcolor, size:32pt)[
    #logflux: A Software Suite For Replicating Results In Automated Log Parsing
  ]

  #v(3em)

  #text(fill: rgb("#4c566a"))[
    #text(size: 26pt)[Guineng Zheng, _Robert Ricci_, and Vivek Srikumar]

    #text(size: 26pt)[ACM REP 2024]
  ]

  #place(bottom+right, dx:0cm, dy:0cm)[#image("University_of_Utah_horizontal_logo.svg", width: 5cm)]
]

#polylux-slide[
  = What is automated log parsing?
  #align(center)[#text(size: 24pt)[Logging statements make up 1.7% to 3.3% of all code]]
  #side-by-side[
    #uncover((beginning:2))[#labelthing("wood-stack.svg","Raw logs")]
  ][
    #uncover((beginning:4))[#labelthing("board.svg", "Structured Data" )]
  ][
    #uncover((beginning:3))[#labelthing("house-frame.svg", "Downstream Task")]
  ]
  #uncover(5)[#labelthing("saw.svg","Automated Parsing")]
]

#polylux-slide[
  = Basic Example
  #v(1fr)
  #well("Input")[
    ```105308 19 INFO pin01dfs.FSNamesystem:pin02 BLOCK* ask pin0310.251.202.181:50010pin04 to delete blk_8860485299141```

    #uncover((beginning:2))[
    ```105312 20 INFO pin05dfs.FSNamesystem:pin06 BLOCK* ask pin0710.252.212.179:50010pin08 to delete blk_8860593299147```]
  ]
  #v(1fr)
  #uncover((beginning:5))[#well("Template")[
    ```<*>   <*> INFO pin09dfs.FSNamesystem:pin10 BLOCK* ask pin11<*>pin12                  to delete <*>```
  ]]
  #v(1fr)
  #uncover((beginning:3))[
    #pinit-highlight(1,2, fill: nordgreen.transparentize(60%))
    #pinit-highlight(5,6, fill: nordgreen.transparentize(60%))
  ]
  #uncover((beginning:4))[
    #pinit-highlight(3,4, fill: nordpurple.transparentize(60%))
    #pinit-highlight(7,8, fill: nordpurple.transparentize(60%))
  ]
  #uncover((beginning:6))[
    #pinit-highlight(9,10, fill: nordgreen.transparentize(60%))
    #pinit-highlight(11,12, fill: nordpurple.transparentize(60%))
    #pinit-arrow(5,9, fill: nordgreen, start-dy: 15pt, start-dx: 30pt, end-dy: -25pt, end-dx: 30pt, stroke: 1pt + nordgreen.darken(25%), thickness: 6pt)
    #pinit-arrow(7,11, fill: nordpurple, start-dy: 15pt, start-dx: 10pt, end-dy: -25pt, end-dx: 10pt, stroke: 1pt + nordpurple.darken(25%), thickness: 6pt)
  ]
]


#polylux-slide[
  = Why is it important to replicate log parsing results?
  - Do I even have to ask‽
  #only(1)[#align(center)[#image("shrug.svg",width: 25%)]]
  #only(2)[
    - Not all literature compares directly to each other
    - Important implementation details not always specified
    - Initial implementations can be wrong, and these problems can compound
    - Practical value of algorithms and high-quality implementations
  ]
]

#polylux-slide[
  = Why is it hard?
  #side-by-side[
    #well("Software")[
      #labelthing("missing.svg","Unavailable", width: 1.3cm)
      #labelthing("maintain.svg","Poor Quality", width: 1.3cm)
      #labelthing("dependencies.svg","Dependency Hell", width: 1.3cm)
    ]
  ][#uncover((beginning:2))[
    #well("Disagreement On")[
      #labelthing("ground-truth.svg","Ground Truth", width: 1.3cm)
      #labelthing("dataset.svg","Datasets", width: 1.3cm)
      #labelthing("timer.svg","Measuring Performance", width: 1.3cm)
    ]
  ]][#uncover((beginning:3))[
    #well("There's More!")[
      #labelthing("runtimes-over-time.svg","Hardware Speed", width: 1.3cm)
      #labelthing("nondeterminism.svg","Nondeterminism", width: 1.3cm)
    ]
   ]]
]

#polylux-slide[
  = What efforts have others made?
  #side-by-side[
    #box(width: 100%)[
    #align(center)[
    #image("amulog-transparent2.png",height: 2.0cm)
    == Amulog
      ]
    ]

    - #link("https://github.com/amulog/amulog")
    - Only implements a few algorithms
    - Design choices prevent implementation of some algorithms
    - Problematic template matching
  ][
    #uncover(2)[
    #box(width:100%)[
    #align(center)[
    #image("logpai-transparent.png",height: 2.0cm)
    == logparser from LOGPAI
        ]
    ]

    #link("https://github.com/logpai/logparser")
    - Hard-to-maintain code
    - Large number of dependencies
    - Some significant bugs
    - Doesn't separate matching and parsing
    ]
  ]
]

#polylux-slide[
  = Designing for Replicability
  #icon("down.svg")[_Low Code Complexity_:]\
  #h(2em) Code needs to be readable by others and maintainable long-term\
  #icon("flexibile.svg")[_High Degree of Flexibility_:]\
  #h(2em) It needs to be possible to add new algorithms as the literature advances\
  #icon("rewind.svg")[_Minimal External Dependencies_:]\
  #h(2em) Dependencies evolve and can easily cause bitrot in your own code
]

#polylux-slide[
  = Some thoughts on software dependencies
  #align(horizon)[
    #side-by-side[
      #labelthing("house-okay.svg",width: 4cm)[What you built]
    ][#uncover((beginning:2))[#align(center + horizon)[#image("fast-time.svg",height: 3cm)]]][
      #uncover(3)[
        #labelthing("house.svg",width: 4cm)[What I Get]
      ]
    ]
  ]
]

#polylux-slide[
  #bigtext(logflux)
]

#polylux-slide[
  = Key repeatability and reproducibility features
  #icon("python.svg")[_Relies on three external Python libraries (numpy, sklearn, torch)_]\
  #h(2em) In comparison to 6 Python libraries for Amulog\
  #h(2em) … and 8 Python libraries, gcc, perl, and more for LOGPAI\
  #icon("big.svg")[_Implements 16 log parsing algorithms_]\
  #h(2em) 12 from-scratch implementations, 3 from Amulog, one from original authors\
  #icon("harness.svg")[_Designed to be called from test harness_]\
  #h(2em) Each algorithm is an independent, stateless function\
  #icon("d20.svg")[_Allows fixed seeds for deterministic results_]
]


#polylux-slide[
  = Easy to install, Easy to use

  #v(1fr)
  #well("Install")[
    ```sh
    pip install logflux git+https://github.com/logflux/logflux
    ```
  ]
  #v(1fr)
  #uncover((beginning:2))[
    #well("Use")[
      ```python
      import logflux

      parser = logflux.AELParser(merge_pct=0.5, min_event_cnt=3)
      templates = parser.parse(logs)
      ```
    ]
  ]
  #v(1fr)
  #uncover(3)[(⁂ also a simple Nix flake)]
]

#polylux-slide[
  //= Algorithms Implemented
  #side-by-side(gutter:2cm)[
    #table(
      columns: (auto, 1fr, 1fr, 1fr),
      stroke: none,
      inset: (x, y) => if y == 0 { (top: 10pt, bottom: 10pt, left: 4pt, right: 4pt ) } else { 4pt },
      fill: (x, y) => if x == 1 {nordsnow2} else if x == 2 { nordsnow1 } else if x == 3 { nordsnow0.lighten(40%) },
      align: (x, y) => if x == 0 {horizon + left} else {horizon + center},
      table.header(
        [],            [#logflux], [#smallcaps[Amulog]], [#smallcaps[LOGPAI]],
      ),
        table.cell(colspan: 4, inset: (top: 20pt), stroke: (bottom: 1pt), align: center)[_Word Frequency_],
        [LFA],                yes,        no,                   yes,
        [LogCluster],         yes,        no,                   orig,
        [SLCT],               yes,        no,                   orig,
        table.cell(colspan: 4, inset: (top: 20pt), stroke: (bottom: 1pt), align: center)[_Clustering_],
        [LenMa],              yes,        yes,                  orig,
        [LKE],                yes,        no,                   yes,
        [LogMine],            yes,        no,                   yes,
        [LogSig],             yes,        no,                   buggy,
        [SHISO],              other,      yes,                  buggy,

      )
  ][
    #table(
      columns: (auto, 1fr, 1fr, 1fr),
      stroke: none,
      inset: (x, y) => if y == 0 { (top: 10pt, bottom: 10pt, left: 4pt, right: 4pt ) } else { 4pt },
      fill: (x, y) => if x == 1 {nordsnow2} else if x == 2 { nordsnow1 } else if x == 3 { nordsnow0.lighten(40%) },
      align: (x, y) => if x == 0 {horizon + left} else {horizon + center},
      table.header(
        [],                    [#logflux], [#smallcaps[Amulog]], [#smallcaps[LOGPAI]]
      ),
        table.cell(colspan: 4, inset: (top: 20pt), stroke: (bottom: 1pt), align: center)[_Structure_],
        [AEL],                 yes,       no,                   yes,
        [Dlog],                other,     yes,                  no,
        [Drain],               yes,       yes,                  yes,
        [FT-tree],             other,     yes,                  no,
        [IPLoM],               yes,       no,                   yes,
        [Spell],               yes,       no,                   yes,
        table.cell(colspan: 4, inset: (top: 20pt), stroke: (bottom: 1pt), align: center)[_Other_],
        [MoLFI],               yes,       no,                   orig,
        [NuLog],               orig,      no,                   orig,

     )
  ]
  #align(center + horizon)[
      #box(move(dy: 3pt,yes)) Independent #h(2em) #box(move(dy: 3pt,orig)) Original #h(2em) #box(move(dy:3pt,other)) Other #h(2em) #box(move(dy: 3pt,buggy)) Buggy
  ]
]


#polylux-slide[
  = Results: Repeatability experiments
  Run each algorithm with\
  #h(2em) #icon("turn-right.svg")[Several input files]\
  #h(4em) #icon("turn-right.svg")[Multiple fixed random seeds]\
  #h(6em) #icon("turn-right.svg")[Multiple times and make sure outputs are always the same]\
  #uncover((2,3))[== Outcome:
    #box(width: 100%)[#place(top + left, dx: 4cm, dy: -1cm)[#image("Eo_circle_green_checkmark.svg", width: 3.5cm)#uncover(3)[(⁂ except NuLog)]]]
  ]
]

#polylux-slide[
  = Results: Training Time on 2K messages
  #v(1fr)
  #box(width: 100%)[
    #image("time_3k_36_9.png", width: 100%)
    #only(2)[#place(top+left, dx:0cm, dy:0cm)[#lorect(1.2cm, 7.0cm)]]
    #only(3)[#place(top+left, dx:10.75cm, dy:6.9cm)[#lorect(1.5cm, 0.6cm)]]
    #only(4)[]
  ]
  #v(1fr)
]

#polylux-slide[
  = Results: Training Time on 20K messages
  #v(1fr)
  #box(width: 100%)[
    #image("time_30k_36_9.png", width: 100%)
    #only(2)[#place(top+left, dx:10.55cm, dy:0cm)[#hirect(1.75cm, 7.5cm)]]
    #only(3)[#place(top+left, dx:2.85cm, dy:0cm)[#hirect(0.8cm, 7.5cm)]]
    #only(4)[#place(top+left, dx:13.40cm, dy:0cm)[#hirect(5.7cm, 7.5cm)]]
    #only(5)[#place(top+left, dx:13.40cm, dy:0cm)[#hirect(2.1cm, 7.5cm)]]
    #only(6)[#place(top+left, dx:17.00cm, dy:0cm)[#hirect(2.1cm, 7.5cm)]]
  ]
  #v(1fr)
]

#polylux-slide[
  = Conclusion
  #well([LogFlux Is Simple To])[
    #set align(center)
    #side-by-side[
      #labelthing("use.svg")[Use]
    ][
      #labelthing("maintain2.svg")[Maintain]
    ][
      #labelthing("extend.svg")[Extend]
    ]
  ]
  #set align(center)
  #set text(size: 24pt)
  It enables large-scale studies independent of the paper authors' code\
  Available at #link("https://github.com/logflux/")
]

#polylux-slide[
  #bigtext[fin]
]
